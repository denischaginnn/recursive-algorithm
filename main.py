def function(array1, summa=0):
    if not isinstance(array1, list):
        print('Вы ввели не массив')
        return None
    if not array1:
        return summa
    summa += array1[0]
    del array1[0]
    return function(array1, summa)

if __name__ == '__main__':
    a = function([1, 2, 3])
    print('Сумма -', a)

