from main import function


def test_function_good():
    assert function([1, 2, 3, 4]) == 10


def test_function_bad():
    assert function([1, 2, 3, 4]) == 9, 'сумма оказалась неправильной'


    # assert function([1, 2, 3, 4]) == 9
